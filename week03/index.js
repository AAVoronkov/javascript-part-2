/**
 * @param {Function[]} operations
 * @param {Function} callback
 */
module.exports = function (operations, callback) {

	if (operations.length == 0) {
		callback(null, []);
	}

	var result = [];
	var operationCounter = 0;
	var errorCounter = 0;

	operations.forEach(function (operation, index) {
		operation(function next(err, data) {
			if (errorCounter != 0) {
				return;
			}
			if (err) {
				callback(err);
				errorCounter++;
				return;
			} else {
				result[index] = data;
				operationCounter++;
			}
			if (operationCounter == operations.length) {
				callback(null, result);
			}	
		});
	});
};