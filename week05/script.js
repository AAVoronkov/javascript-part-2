'use strict';

function validateForm(args) {

	var isValidString = function (value, pattern) {
		if (!value) {
			return true;
		} 
		var regExpValidator = new RegExp(pattern || /^[а-яa-z]+$/i);

		return regExpValidator.test(value);
	}

	var isValidNumber = function (value, min, max) {
		if (!value) {
			return true;
		}	

		var numberValue = parseInt(value);

		if (isNaN(numberValue)) {
			return false;
		}
		if (min && parseInt(min) > value) {
			return false;
		}
		if (max && parseInt(max) < value) {
			return false;
		}

		return true;
	}

	var isValidInput = function (input) {
		if (input.dataset.hasOwnProperty('required') && !input.value) {
			return false;
		}

		if (input.dataset.hasOwnProperty('validator')) {
			switch (input.dataset.validator) {
				case 'letters':
					return isValidString(input.value);
				case 'number':
					return isValidNumber(input.value, input.dataset.validatorMin, input.dataset.validatorMax);
				case 'regexp':
					return isValidString(input.value, input.dataset.validatorPattern);
				default:
					return true;
			}
		}
	}

	var formElem = document.getElementById(args.formId);

	formElem.addEventListener('focus', function (event) {
		if (event.target.tagName === 'INPUT') {
			if (event.target.classList.contains(args.inputErrorClass)) {
				event.target.classList.remove(args.inputErrorClass);
			}
		}
	}, true);

	formElem.addEventListener('blur', function (event) {
		if (event.target.tagName === 'INPUT') {
			if (!isValidInput(event.target)) {
				event.target.classList.add(args.inputErrorClass);
			}
		}
	}, true);

	var isFormValid = function () {
		var elems = document.querySelectorAll('#' + args.formId + ' input');
		var elemsList = Array.prototype.slice.call(elems);
		var isValidBool = true;
		elemsList.forEach(function (elem) {
			if (!isValidInput(elem)) {
				isValidBool = false;
				elem.classList.add(args.inputErrorClass);
			}
		});
		return isValidBool;
	}

	formElem.addEventListener('submit', function (event) {
		event.preventDefault();
		formElem.classList.remove(args.formInvalidClass, args.formValidClass);
		if (isFormValid()) {
			formElem.classList.add(args.formValidClass);
		} else {
			formElem.classList.add(args.formInvalidClass);
		}
	});
}