module.exports = Collection;


/**
 * Конструктор коллекции
 * @constructor
 */
function Collection() {
	this.content = [];
}


// Методы коллекции
Collection.prototype.values = function() {
	return this.content;
};

Collection.prototype.count = function() {
	return this.content.length;
};

Collection.prototype.at = function(index) {
	if ((index <= 0) || (index > this.content.length)) {
		return null;
	} else {
		return this.content[index - 1];
	} 
};

Collection.prototype.append = function(data) {
	if (data instanceof Collection) {
		this.content = this.content.concat(data.content);
	}
	else {
		this.content.push(data);
	} 
};

Collection.prototype.removeAt = function(index) {
	if ((index <= 0) || (index > this.content.length)) {
		return false;
	} else {
		this.content.splice((index - 1), 1);
		return true;
	} 
};


/**
 * Создание коллекции из массива значений
 */
Collection.from = function(data) {
	var collection = new Collection();
	collection.content = collection.content.concat(data);
	return collection;
};
